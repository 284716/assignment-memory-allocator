#define _DEFAULT_SOURCE
#include "util.h"

_Noreturn void err( const char* msg, ... ) {
  va_list args;
  va_start (args, msg);
  vfprintf(stderr, msg, args);
  va_end (args);
  abort();
}

size_t pages_count   ( size_t mem ) { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
size_t round_pages   ( size_t mem ){ return getpagesize() * pages_count( mem ) ; }

extern inline size_t size_max( size_t x, size_t y );
