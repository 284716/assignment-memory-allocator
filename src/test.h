#ifndef _TESTS_H_
#define _TESTS_H_
#include "mem.h"
#include "mem_internals.h"
#define TESTS_COUNT 4

bool simple_malloc_free_test(struct block_header* heap);
bool multiple_malloc_free_test(struct block_header* heap);
bool new_region_test(struct block_header* heap);
bool new_region_no_extends_test(struct block_header* heap);


#endif //_TESTS_H_

