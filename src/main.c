#include "mem.h"
#include "test.h"

bool (*tests_mas[TESTS_COUNT])(struct block_header*)={
	simple_malloc_free_test,
	multiple_malloc_free_test,
	new_region_test,
	new_region_no_extends_test
};
static void print_start(uint32_t n){
	printf("\033[0;33m");//yellow text
	printf("    ----- start test %u -----\n",n);
	printf("\033[0m");//normal color
}
static void print_success(uint32_t n){
	printf("\033[0;32m");//green text
	printf("    ----- succesfully end test %u -----\n",n);
	printf("\033[0m");//normal color
}
static void print_fail(uint32_t n){
	printf("\033[0;31m");//red text
	printf("Fail on test %u\nNot all tests passed",n);
	printf("\033[0m");//normal color
}
static int run_tests(void* heap){
	for(uint32_t i = 0;i < TESTS_COUNT; ++i){
		print_start(i);
		if(! (*tests_mas[i])(heap)){
			print_fail(i);
			return 1;
		}
		print_success(i);
	}
	printf("All tests passed!\n");
	return 0;
}
int main(){
	void* heap=heap_init(0);
	debug_heap(stdout,heap);
	run_tests(heap);
	return 0;
}

