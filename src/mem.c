#define _DEFAULT_SOURCE

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool            block_is_big_enough( size_t query, struct block_header* block ) { return block->capacity.bytes >= query; }
static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , -1, 0 );
}

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

extern inline bool region_is_invalid( const struct region* r );




/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region  ( void const * addr, size_t query ) {
	size_t size = region_actual_size(query + BLOCK_HEADER_SIZE);
  	bool extends = true;
  	void* region_addr = map_pages(addr,size, MAP_FIXED_NOREPLACE);
  	if(region_addr == MAP_FAILED){
  		extends = false;
		region_addr = map_pages(addr, size, 0);
		if(region_addr == MAP_FAILED)
	  		return REGION_INVALID;
	}
  	block_init(region_addr, (block_size) {.bytes = size}, NULL);
  	return (struct region){.addr=region_addr, .size = size, .extends = extends };
}

static void* block_after( struct block_header const* block )         ;

void* heap_init( size_t initial ) {
  const struct region region = alloc_region( HEAP_START, initial );
  if ( region_is_invalid(&region) ) return NULL;

  return region.addr;
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable( struct block_header* restrict block, size_t query) {
  return block-> is_free && query + BLOCK_HEADER_SIZE + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}
static bool split_if_too_big( struct block_header* block, size_t query ) {
	if(block_splittable(block, query)){
		block_init(
				block->contents + query, 
				(block_size){block->capacity.bytes - query},
				block->next);
		block->next = (struct block_header*)(block->contents + query);
		block->capacity.bytes = query;
		return true;
	}
	return false;
}


/*  --- Слияние соседних свободных блоков --- */

static void* block_after( struct block_header const* block )              {
  return  (void*) (block->contents + block->capacity.bytes);
}
static bool blocks_continuous (
                               struct block_header const* fst,
                               struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
  return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}
static bool try_merge_with_next( struct block_header* block ) {
	if(block != NULL && block->next !=NULL){
	if (mergeable(block, block->next)){
		block->capacity.bytes += size_from_capacity(block->next->capacity).bytes;
		block->next = block->next->next;
		return true;
	}
	}
	return false;
}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
  enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
  struct block_header* block;
};

static struct block_search_result find_good_or_last  ( struct block_header* restrict block, size_t sz )    {
	if(block == NULL) 
		return (struct block_search_result){
			.type = BSR_CORRUPTED,
			.block = block};
	while(true){	//я не люблю такие циклы, но я не придумал лучше
		while(try_merge_with_next(block)){}
       		if(block->is_free && block_is_big_enough(sz,block))
			return (struct block_search_result){
				.type = BSR_FOUND_GOOD_BLOCK, 
				.block = block};
		if(block->next==NULL) 
			break;
		block=block->next;
	}	
	return (struct block_search_result){
		.type = BSR_REACHED_END_NOT_FOUND, 
		.block = block};
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) {
	size_t size = size_max(query, BLOCK_MIN_CAPACITY);
	struct block_search_result result = find_good_or_last(block, size);
	if(result.type == BSR_FOUND_GOOD_BLOCK){
		split_if_too_big(result.block, size);
		result.block->is_free = false;
	}
	return result;
}


static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {
  /*  ??? */
	struct region new_region = alloc_region(block_after(last), query);
	if(region_is_invalid(&new_region))
		return NULL;
	last->next = new_region.addr;
	if(new_region.extends && last->is_free){	//на самом деле try_merge_with_next автоматически проверяет следуют ли блоки друг за другом и сообщит нам об этом. Но раз нам дали region.extends давайте его использовать
		try_merge_with_next(last);
		return last;
	}
	return new_region.addr;
}
/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc( size_t query, struct block_header* heap_start) {
	struct block_search_result result = try_memalloc_existing(query, heap_start);
	switch(result.type){
		case BSR_FOUND_GOOD_BLOCK: return result.block;
		case BSR_REACHED_END_NOT_FOUND:
			heap_start = grow_heap(result.block, query);	//переиспользование вмксто выделения без причины
			if(heap_start !=NULL)
				return try_memalloc_existing(query, heap_start).block;
		default:break;
	}
	return NULL; 	//сюда мы хитро проваливаемся если не получилось ни выделить ни расширить
}

void* _malloc( size_t query ) {
  struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
  if (addr) return addr->contents;
  else return NULL;
}

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-BLOCK_HEADER_SIZE);
}
void _free( void* mem ) {
  if (!mem) return ;
  struct block_header* header = block_get_header( mem );
  header->is_free = true;
  while( try_merge_with_next(header));
}
