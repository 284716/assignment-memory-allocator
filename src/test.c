#define _DEFAULT_SOURCE
#include "mem_internals.h"
#include "mem.h"
#include "test.h"
#include "util.h"
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

static void err_msg(){
	fprintf(stderr,"ERROR! test aborted \n");
}
static struct block_header* block_header_from_contents(void* contents){
	return (struct block_header*)((uint8_t*)contents - BLOCK_HEADER_SIZE);
}
static bool is_free_from_contents(void* contents){
	return block_header_from_contents(contents)->is_free;
}
static size_t capacity_from_contents(void* contents){
	return block_header_from_contents(contents)->capacity.bytes;
}
static void* block_after(struct block_header const* block) {
	  return  (void*) (block->contents + block->capacity.bytes);
}
static bool blocks_continuous (struct block_header const* fst, struct block_header const* snd) {
	  return (void*)snd == block_after(fst);
}
static void* _malloc_test(const size_t size, struct block_header* heap){
	printf("calling _malloc(%zu)...\n",size);
	void * ptr = _malloc(size);
	printf("returned pointer: %p\n",ptr);
	debug_heap(stdout,heap);
	if(ptr == NULL){
		err_msg();
		err("_malloc(%zu) returned NULL pointer",size);
		return NULL;
	}
	if(is_free_from_contents(ptr)){
		err_msg();
		err("_malloc(%zu) returned free block of memory");
		return NULL;
	}
	if(capacity_from_contents(ptr) != size){
		err_msg();
		err("_malloc(%zu) returned block with capasity == %zu", capacity_from_contents(ptr));
		return NULL;
	}
	return ptr;
}
static bool _free_test(void* ptr, struct block_header* heap){
	printf("calling _free(%p)...\n", ptr);
	_free(ptr);
	debug_heap(stdout, heap);
	if(!is_free_from_contents(ptr)){
		err_msg();
		err("block of memory not free after _free");
		return false;
	}	
	printf("Pointer %p now is free\n",ptr);
	return true;
}
static bool check_heap(struct block_header* heap){
	_free(heap->contents);
	debug_heap(stdout,heap);
	if(! heap->is_free){//я не знаю насколько это правильно, но вообще как буд-то бы надо
		err_msg();
		err("Heap is not free.\nLounch this test without any allocation before. Or clear heap using free() on all allocations");
		return false;
	}
	return true;
}
bool simple_malloc_free_test(struct block_header* heap){
	if(! check_heap(heap)) 
		return false;
	const size_t size = 1000;
	void* ptr = _malloc_test(size,heap);
	if(!ptr)
		return false;
	if(! _free_test(ptr,heap))
		return false;
	return true;
}
bool multiple_malloc_free_test(struct block_header* heap){
	if(! check_heap(heap))
		return false;
	const size_t size1 = 1001;
	const size_t size2 = 1002;
	const size_t size3 = 1003;
	void* ptr1 = _malloc_test(size1, heap);
	void* ptr2 = _malloc_test(size2, heap);
	void* ptr3 = _malloc_test(size3, heap);
	if(!ptr1 || !ptr2 || !ptr3){ 
		//писать каждый раз _free на каждую ошибку чёт некрасиво. Очень хочется использовать goto на 'специальный return' но совесть не позволяет. 
		_free(ptr1);
		_free(ptr2);
		_free(ptr3);
		return false;
	}
	if(! _free_test(ptr2, heap)){
		_free(ptr1);
		_free(ptr3);
		return false;
	}
	if(is_free_from_contents(ptr1) || is_free_from_contents(ptr3)){
		err_msg();
		_free(ptr1);
		_free(ptr3);
		err("one call _free has an effect on neighboring blocks\n");
	       return false;	
	}
	if( ! _free_test(ptr1, heap)){
		_free(ptr3);
		return false;
	}
	if(capacity_from_contents(ptr1) == size1){
		err_msg();
		_free(ptr3);
		err("two neighboring free blocks not merged after call _free() on first\n");
		return false;
	}
	if( ! _free_test(ptr3,heap))
		return false;
	return true;
}
static struct block_header* last_block(struct block_header* heap){
	while (heap->next) heap = heap->next;
	return heap;
}
bool new_region_test(struct block_header* heap){
	if(! check_heap(heap)) 
		return false;
	const size_t size1 = last_block(heap)->capacity.bytes;
	const size_t size2 = 1000;
	void* ptr1 = _malloc_test(size1, heap);
	void* ptr2 = _malloc_test(size2, heap);
	if(!ptr1 || !ptr2){
		_free(ptr1);
		_free(ptr2);
		return false;
	}
	if(!blocks_continuous(	block_header_from_contents(ptr1),
				block_header_from_contents(ptr2) )){
		err_msg();
		_free(ptr1);
		_free(ptr2);
		err("second block, allcated in new region isn't the next of first\n");
		return false;
	}
	if(!_free_test(ptr1, heap)){
		_free(ptr2);
		return false;
	}
	if(!_free_test(ptr2, heap)){
		return false;
	}
	return true;
}
static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , -1, 0 );
}
bool new_region_no_extends_test(struct block_header* heap){
	if(! check_heap(heap)) 
		return false;
	const size_t size1 = last_block(heap)->capacity.bytes;
	const size_t size2 = 1000;
	void* ptr1 = _malloc_test(size1, heap);
	if(!ptr1)
		return false;
	printf("creating not free memory after last block...\n");
	printf("reserve memory on %p\n",map_pages((void*)round_pages((size_t)block_after(last_block(heap))), 1000, MAP_FIXED));// да,  я видел что у других MAP_FIXED_NOREPLACE, но по каким-то причинам на моей машинке MAP_FIXED_NOREPLACE возвращает NULL. Я это проверял, и тесты в ./tester у меня на локалке не проходят. Чтобы тестить у себя свой код я сделал MAP_FIXED. технически можно запустить map_pages вообще без флагов, и если место после блока не занято, то мы его займём. А если оно каким-то чудом уже занято, то это всё ещё нам на руку
	void* ptr2 = _malloc_test(size2, heap);
	if(!ptr2){
		_free(ptr1);
		return false;
	}

	if(blocks_continuous(	block_header_from_contents(ptr1),
				block_header_from_contents(ptr2) )){
		err_msg();
		_free(ptr1);
		_free(ptr2);
		err("second block, allcated in new region somehow located next of first over taken memory\n");
		return false;
	}
	if(!_free_test(ptr1, heap)){
		_free(ptr2);
		return false;
	}
	if(!_free_test(ptr2, heap)){
		return false;
	}
	return true;

}

