#ifndef _UTIL_H_
#define _UTIL_H_

#include <assert.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <unistd.h>

inline size_t size_max( size_t x, size_t y ) { return (x >= y)? x : y ; }

size_t          pages_count   ( size_t mem );
size_t          round_pages   ( size_t mem );

_Noreturn void err( const char* msg, ... );


#endif
